/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Img.h
 * Author: Krypton0
 *
 * Created on 1. dubna 2017, 1:07
 */

#ifndef IMG_H
#define IMG_H

#include "Header.h"
#include <openssl/evp.h>

// jak velký blok (kolik charů) se má šifrovat
#define BLOCK_SIZE 512

/* zkratky:
 * o-ssl = "open ssl"
 */

using namespace std;

class Img {
public:
    /**
     * 
     * @param header třída obsahující hlavičková data
     * @param src vstupní proud s daty obrázku (začína na pozici "zac" - začátek obrazových dat)
     * @param cipher šifra, která se má použít k šifrování
     */
    Img(const Header &header, fstream &src, const char* cipher);
    //Img(const Img& orig);
    virtual ~Img();

    /**
     * šifrování obrázku (algoritmus jako přes kopírák z předcházející úlohy)
     * - rozdíl oproti předcházejícímu algoritmu je, že obrázek se šifruje a zapisuje postupně
     * @param src
     */
    void encrypt(fstream &src);
    // =encrypt, pouze změna o-ssl názvů funkcí z encrypt na decrypt
    void decrypt(fstream &src);

    //vypíše data do souboru (sloučí header a zpracovaná obrazová data)
    void print_out(const char *out);

private:

    /*
     * pole s daty pro zašifrování / dešifrování
     * - zpracová vá se vždy jen BLOCK_SIZE velikost - větší je pouze kvůli paddingu
     * */
    unsigned char input[2 * BLOCK_SIZE];
    //tohle už asi nic nedělá :) 
    unsigned char *processed = NULL;
    //délka obrazových dat vstupního souboru
    unsigned input_lenght;
    // velikost obrazových data
    // změní se po zavolání funkcí encrypt/decrypt
    unsigned counted_length = 0;

    Header header;

    //index šifry v o-ssl
    const EVP_CIPHER * cipher;



};

#endif /* IMG_H */

