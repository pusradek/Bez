/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Img.cpp
 * Author: Krypton0
 * 
 * Created on 1. dubna 2017, 1:07
 */

#include "Img.h"

const EVP_CIPHER* getCipher(const char* cipherName) {
    
    if (strcmp("ecb", cipherName) == 0)return EVP_des_ecb();
    if (strcmp("cbc", cipherName) == 0)return EVP_des_cbc();

    printf("Sifra %s neexistuje.\n", cipherName);
    exit(1);
}

unsigned char* strCpy(const char* str, unsigned lenght) {
    unsigned char *array = new unsigned char [lenght + 1];
    for (unsigned i = 0; i < lenght; i++) {
        array[i] = (unsigned char) str[i];
    }
    array[lenght] = '\0';
    return array;
}

void Img::print_out(const char *out) {
    ofstream dst(out);
    if (!dst.good()) {
        exit(1);
    }
    dst << header.header;

    fstream src("tmp");
    if (!src.good()) {
        cout << "Corrupted data" << endl;
        exit(1);
    }
    
    for (char c = src.get(); src.good(); c=src.get())dst << c;

    dst.close();
    src.close();
    remove("tmp");
}

void Img::decrypt(fstream &src) {
    ofstream dst("tmp");
    if (!dst.good()) {
        exit(1);
    }
    unsigned char key[EVP_MAX_KEY_LENGTH] = "Muj klic"; // klic pro sifrovani
    unsigned char iv[EVP_MAX_IV_LENGTH] = "inicial. vektor"; // inicializacni vektor

    //hard coded
    unsigned char ot[2*BLOCK_SIZE]; // sifrovany text

    //int otLength = data.length();
    int otLength = 0;
    //int stLength = input_lenght;
    int tmpLength = 0;

    EVP_CIPHER_CTX ctx; // struktura pro kontext

    /* Desifrovani */
    EVP_DecryptInit(&ctx, cipher, key, iv); // nastaveni kontextu pro sifrovani //evp.h - line 744
    for (unsigned int i = BLOCK_SIZE; i <= input_lenght && input_lenght >= i; i += BLOCK_SIZE) {
        for (int i = 0; i < BLOCK_SIZE; i++) {
            input[i]=src.get();
        }
        EVP_DecryptUpdate(&ctx, ot, &tmpLength, input, BLOCK_SIZE);
        otLength += tmpLength;
        for (int j = 0; j < tmpLength; j++) {
            dst << ot[j];
        }
    }
    for (unsigned i = 0; i < input_lenght % BLOCK_SIZE; i++)input[i]=src.get();
    for (char c = src.get(); src.good(); c=src.get())dst << c;
    
    EVP_DecryptUpdate(&ctx, ot, &tmpLength, input, input_lenght % BLOCK_SIZE);
    otLength += tmpLength;
    for (int j = 0; j < tmpLength; j++) {
        dst << ot[j];
    }
    EVP_DecryptFinal(&ctx, ot, &tmpLength); // dokonceni (ziskani zbytku z kontextu)
    for (int j = 0; j < tmpLength; j++) {
        dst << ot[j];
    }
    otLength += tmpLength;

    dst.close();
    
    //cout << "Dešifrováno " << otLength << " znaků" << endl;
    counted_length = otLength;
    this->header.change_length(this->counted_length - this->input_lenght);
    this->processed = ot;

}

void Img::encrypt(fstream &src) {
    ofstream dst("tmp");
    if (!dst.good()) {
        exit(1);
    }
    //posibilities
    unsigned char key[EVP_MAX_KEY_LENGTH] = "Muj klic"; // klic pro sifrovani
    unsigned char iv[EVP_MAX_IV_LENGTH] = "inicial. vektor"; // inicializacni vektor

    //hard coded
    unsigned char st[2*BLOCK_SIZE]; // sifrovany text
    
    int stLength = 0;
    int tmpLength = 0;

    EVP_CIPHER_CTX ctx; // struktura pro kontext

    /* Sifrovani */
    EVP_EncryptInit(&ctx, cipher, key, iv); // nastaveni kontextu pro sifrovani //evp.h - line 744
    for (unsigned int i = BLOCK_SIZE; i <= input_lenght && input_lenght >= i; i += BLOCK_SIZE) {
        for (int i = 0; i < BLOCK_SIZE; i++) {
            input[i]=src.get();
        }
        EVP_EncryptUpdate(&ctx, st, &tmpLength, input, BLOCK_SIZE);
        stLength += tmpLength;
        for (int j = 0; j < tmpLength; j++) {
            dst << st[j];
        }
    }
    for (unsigned i = 0; i < input_lenght % BLOCK_SIZE; i++)input[i]=src.get();
    for (char c = src.get(); src.good(); c=src.get())dst << c;
    
    EVP_EncryptUpdate(&ctx, st, &tmpLength, input, input_lenght % BLOCK_SIZE);
    stLength += tmpLength;
    for (int j = 0; j < tmpLength; j++) {
        dst << st[j];
    }
    EVP_EncryptFinal(&ctx, st, &tmpLength); // dokonceni (ziskani zbytku z kontextu)
    for (int j = 0; j < tmpLength; j++) {
        dst << st[j];
    }
    stLength += tmpLength;

    dst.close();

    //cout << "Input " << this->input_lenght << " znaků" << endl;
    //cout << "Zašifrováno " << stLength << " znaků" << endl;
    counted_length = stLength;
    this->header.change_length(this->counted_length - this->input_lenght);
    dst.close();
    this->processed = st;
}

Img::Img(const Header &header, fstream &src, const char* cipher) :
input_lenght(header.file_lenght-header.zac), header(header), cipher(getCipher(cipher)){
}

Img::~Img() {
}

