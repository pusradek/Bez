/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Krypton0
 *
 * Created on 31. března 2017, 21:51
 */

#include <cstdlib>
#include <iostream>
#include <istream>
#include <fstream>
#include "Header.h"
#include "Img.h"

using namespace std;

#define homer "ref/homer-simpson.bmp"
#define scientist "ref/Mad_scientist.bmp"

#define homer_ecb "ref/homer-simpson_ecb.bmp"
#define homer_cbc "ref/homer-simpson_cbc.bmp"

#define scientist_ecb "ref/Mad_scientist_ecb.bmp"
#define scientist_cbc "ref/Mad_scientist_cbc.bmp"

#define d1 "ref/bad_homer-simpson1.bmp"
#define d2 "ref/bad_homer-simpson2.bmp"
#define d3 "ref/bad_homer-simpson3.bmp"
#define d4 "ref/bad_homer-simpson4.bmp"
/*
 * 
 */
void getName(string &src);
void printl(string &str);

struct settings {
    string mod; // = ecb / cbc
    char process = 'F'; // E -> encrypt file, D -> decrypt file, F -> ERR (fail)
    string output;
};
settings get_argument(int argc, char **argv);
void set_suffix(string name, settings &sett);

int main(int argc, char** argv) {
    //blok pro vložení (testování) argumentů
    /*char **arr;
    arr = new char*[2];
    arr[0] = new char[4];
    strncpy(arr[0], "-d", 3);
    arr[1] = new char[4];
    strncpy(arr[1], "cbc", 4);
    settings arg = get_argument(2, arr);
    delete []arr[0]; delete []arr[1]; delete []arr;*/

    settings arg = get_argument(argc, argv);
    string src_name = homer_cbc;

    cout << "Vlož jméno obrázku k (de)šifrování" << endl;

    //zakomentovat následující řádky k "define" řešení
    src_name.clear();
    getName(src_name);


    fstream src(src_name);
    if (!src.good()) {
        cout << "Missing / corrupted data" << endl;
        return 1;
    }
    //přípona
    set_suffix(src_name, arg);

    Header header(src);
    Img img(header, src, arg.mod.c_str());
    arg.process == 'E' ? img.encrypt(src) : img.decrypt(src);
    img.print_out(arg.output.c_str());

    cout << "Hotovo!" << endl;
    return 0;
}

void set_suffix(string name, settings &sett) {
    if (name.length() < 5) {
        cout << "špatné jméno" << endl;
        exit(1);
    }
    if (name.back() != 'p') {
        cout << "špatné jméno" << endl;
        exit(1);
    }
    name.pop_back();
    if (name.back() != 'm') {
        cout << "špatné jméno" << endl;
        exit(1);
    }
    name.pop_back();
    if (name.back() != 'b') {
        cout << "špatné jméno" << endl;
        exit(1);
    }
    name.pop_back();
    if (name.back() != '.') {
        cout << "špatné jméno" << endl;
        exit(1);
    }
    name.pop_back();

    sett.output = name + sett.output;
}

void out(settings &arg) {
    if (arg.process == 'D') {
        arg.output = "_dec.bmp";
        return;
    }
    if (arg.mod == "ecb")arg.output = "_ecb.bmp";
    else arg.output = "_cbc.bmp";
}

settings get_argument(int argc, char **argv) {
    bool mod = false,
            process = false;

    settings setting;

    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "ecb") == 0 || strcmp(argv[i], "cbc") == 0) {
            if (mod) {
                cout << "příliš mnoho módů" << endl;
                exit(1);
            }
            mod = true;
            setting.mod = argv[i];
            continue;
        }
        if (strcmp(argv[i], "-e") == 0) {
            if (process) {
                cout << "příliš mnoho d/e přepínačů" << endl;
                exit(1);
            }
            process = true;
            setting.process = 'E';
            continue;
        }
        if (strcmp(argv[i], "-d") == 0) {
            if (process) {
                cout << "příliš mnoho d/e přepínačů" << endl;
                exit(1);
            }
            process = true;
            setting.process = 'D';
            continue;
        }
    }

    if (setting.process == 'F' || setting.mod.empty()) {
        cout << "špatně zadané přepínače" << endl;
        exit(1);
    }

    out(setting);

    return setting;
}

void getName(string &src) {
    for (char c = cin.get(); c != '\n' && !cin.eof(); c = cin.get()) {
        src += c;
    }
}

void printl(string &str) {
    cout << str << endl;
}