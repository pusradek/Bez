/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Header.h
 * Author: Krypton0
 *
 * Created on 31. března 2017, 21:52
 */

#ifndef HEADER_H
#define HEADER_H
#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

class Header {
public:
    Header(fstream &src);
    Header(const Header& orig);
    virtual ~Header();
    void change_length(int d);
    
    bool failBit=false;
    string header;
    uint32_t zac;
    
    
    uint32_t file_lenght=0;
    
};

#endif /* HEADER_H */

