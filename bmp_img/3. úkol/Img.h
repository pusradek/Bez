/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Img.h
 * Author: Krypton0
 *
 * Created on 1. dubna 2017, 1:07
 */

#ifndef IMG_H
#define IMG_H

#include "Header.h"
#include <openssl/evp.h>

#define BLOCK_SIZE 512

using namespace std;

class Img {
public:
    Img(const Header &header, fstream &src, const char* cipher);
    //Img(const Img& orig);
    virtual ~Img();

    void encrypt(fstream &src);
    void decrypt(fstream &src);
    
    void print_out(const char *out);

private:
    
    unsigned char input[2*BLOCK_SIZE];
    unsigned char *processed=NULL;
    unsigned input_lenght;
    unsigned counted_length=0;
    
    Header header;
    
    const EVP_CIPHER * cipher;
    
    

};

#endif /* IMG_H */

