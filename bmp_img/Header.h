/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Header.h
 * Author: Krypton0
 *
 * Created on 31. března 2017, 21:52
 */

#ifndef HEADER_H
#define HEADER_H
#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

class Header {
public:
    /**
     * načte hlavičku souboru a zároveň zkontroluje její data
     * @param src vstupní proud obrázku
     */
    Header(fstream &src);
    Header(const Header& orig);
    virtual ~Header();
    /**
     * změní informaci o délce dat obrázku o nějaké "delta"
     * - u šifrování se délka zvětšuje a u dešifrování změnšuje (padding)
     * 
     * @param d o kolik mám být zvětšena / zmenšena délka obrázku
     */
    void change_length(int d);
    
    //true pokud došlo k nějaké neplatné operaci
    bool failBit=false;
    //data z hlavičky souboru
    string header;
    //začátek obrazových dat
    uint32_t zac;
    
    //délka souboru s obrázkem
    uint32_t file_lenght=0;
    
};

#endif /* HEADER_H */

