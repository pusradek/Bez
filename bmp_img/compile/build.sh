#! /bin/bash
echo "copying...";
rm *.cpp *.h *.gch *.out *.bmp 2>/dev/null
cp ../*.cpp  ../*.h  .
echo "building..."
g++ -pedantic -Wall -g -std=c++1y *.h *.cpp -lcrypto -lssl
