/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Header.cpp
 * Author: Krypton0
 * 
 * Created on 31. března 2017, 21:52
 */

#include "Header.h"

void Header::change_length(int d){//difference
    this->file_lenght+=d;
    
    unsigned x=file_lenght & 0xff;
    this->header.at(2)=(char) x;
    
    x=file_lenght>>2*4;
    x=x & 0xff;
    this->header.at(3)=(char) x;
    
    x=file_lenght>>4*4;
    x=x & 0xff;
    this->header.at(4)=(char) x;
    
    x=file_lenght>>6*4;
    x=x & 0xff;
    this->header.at(5)=(char) x;
    
}

uint32_t calcSize(fstream &src, string &header) {

    const unsigned c1 = src.get(),
            c2 = src.get(),
            c3 = src.get(),
            c4 = src.get();
    
    header+=(char) c1;
    header+=(char) c2;
    header+=(char) c3;
    header+=(char) c4;

    uint32_t u = c4;
    u <<= 8;
    u += c3;
    u <<= 8;
    u += c2;
    u <<= 8;
    u += c1;
    return u;
}

uint32_t systemSize(fstream &src) {
    src.seekg(0, src.end);
    const uint32_t length = src.tellg();
    src.seekg(0, src.beg);
    return length;
}

Header::Header(fstream &src) {

    const uint32_t lenght = systemSize(src); //lenght of file
    file_lenght=lenght;
    //cout << "file lenght: " << lenght << endl;

    header = src.get();
    header += src.get();

    if (header != "BM" || src.fail()) {
        cout << "nezačína na \"BM\"" << endl;
        failBit = true;
        exit(1);
    }

    if (calcSize(src, header) != lenght || lenght<15) {
        cout << "špatná délka" << endl;
        failBit = true;
        exit(1);
    }
    
    for (int i = 0; i < 4; i++) {
        header+=src.get();
    }
    
    zac=calcSize(src, header);
    if(zac>=file_lenght){
        cout << "špatný začátek obrázku" << endl;
        exit(1);
    }
    for (uint32_t i = 0; i < zac-14; i++) {
        header+=src.get();
    }
    
    //cout << "zac: " << zac << endl;

}

Header::Header(const Header& orig) {
    this->file_lenght=orig.file_lenght;
    this->header=orig.header;
    this->zac=orig.zac;
    this->failBit=orig.failBit;
}

Header::~Header() {
}

