#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
//#include <time.h>

int main(int argc, char *argv[]) {

    int i;
    //char text[] = "Text pro hash.";
    char text[20];
    
    char hashFunction[] = "sha1"; // zvolena hashovaci funkce ("sha1", "md5" ...)

    EVP_MD_CTX ctx; // struktura kontextu
    const EVP_MD *type; // typ pouzite hashovaci funkce
    unsigned char hash[EVP_MAX_MD_SIZE]; // char pole pro hash - 64 bytu (max pro sha 512)


    int length; // vysledna delka hashe

    /* Inicializace OpenSSL hash funkci */
    OpenSSL_add_all_digests();
    /* Zjisteni, jaka hashovaci funkce ma byt pouzita */
    type = EVP_get_digestbyname(hashFunction);

    /* Pokud predchozi prirazeni vratilo -1, tak nebyla zadana spravne hashovaci funkce */
    if (!type) {
        printf("Hash %s neexistuje.\n", hashFunction);
        exit(1);
    }

    /* Provedeni hashovani */

    //srand(time(NULL));
    FILE *randomData = fopen("/dev/urandom", "r");
    if(randomData==NULL){
        printf("Nelze cist /dev/urandom");
        return 0;
    }

    int ii;
    for (ii = 0; 1; ii++) {

        int iii = 0;
        for (iii = 0; iii < 19; iii++) {
            text[iii] = (char) fgetc(randomData);
        }
        text[19] = '\0';

        EVP_DigestInit(&ctx, type);
        EVP_DigestUpdate(&ctx, text, strlen(text)); // zahashuje text a ulozi do kontextu
        EVP_DigestFinal(&ctx, hash, (unsigned int *) &length); // ziskani hashe z kontextu

        //if (hash[0] == 0xca && hash[1] == 0xfe)break;
        if (hash[0] == 0xaa || hash[0] == 0xbb)break;
    }
    
    
    fclose(randomData);

    /* Vypsani vysledneho hashe */
    printf("Hash textu \"%s\" je: ", text);
    for (i = 0; i < length; i++) {
        printf("%02x", hash[i]);
    }
    printf("\n");
    
    
    
    exit(0);
}

/*char * randomize(char * a) {
    
    char myRandomData[20];
    size_t randomDataLen = 0;
    while (randomDataLen < sizeof myRandomData) {
        ssize_t result = fread(randomData, myRandomData + randomDataLen, (sizeof myRandomData) - randomDataLen);
        if (result < 0) {
            // error, unable to read /dev/random 
        }
        randomDataLen += result;
    }
    fclose(randomData);
}*/