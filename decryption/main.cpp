/* 
 * File:   main.cpp
 * Author: Krypton0
 *
 * Created on 20. března 2016, 4:05
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int set(char);
string to_text(string, string);

int main(int argc, char** argv) {
    string first = "F3E373DA2398B3349FFB3DB0DB3732A2AFEC1E0D0735C7A93080ED5C0490", //default strings
            second = "C8EA65CD6697F43293FB37B7CF7929B7A6EA4D031330D9B73B95AB0C428D",
            xor_fs,
            xor_all,
            translate_message,
            message="abcdefghijklmnopqrstuvwxyz0123"; // default message
    
    cin >> first >> second;
    
    //-----------------------------------------------------------
    xor_fs=to_text(first, second); // CT_1 xor CT_2
    
    //-------------------------------------------------------
    
    for (int i = 0, a, b,c; i < xor_fs.length(); i++) {
        
        c=xor_fs.at(i) ^ message.at(i);
        cout<< (char) c;
    }
    return 0;
}

//--------------------------------------------------------------------------------------- to_textt

string to_text(string first, string second) {

    string result;
    int a = 0, b = 0;

    if (first.length() != second.length()) {
        cerr << "Různě dlouhé řetězce" << endl;
        exit(0);
    }

    

    for (int i = 0; i < first.length(); i += 2) {

        a = set(first.at(i));
        b = set(second.at(i));

        if (i + 1 <= first.length()) {

            a <<= 4;
            b <<= 4;

            a += set(first.at(i + 1));
            b += set(second.at(i + 1));
        }

        result += (char) (a ^ b);
    }

    
    
    return result;
}

int set(char c) {

    c = tolower(c);

    switch (c) {

        case '0': return 0;
        case '1': return 1;
        case '2': return 2;
        case '3': return 3;
        case '4': return 4;
        case '5': return 5;
        case '6': return 6;
        case '7': return 7;
        case '8': return 8;
        case '9': return 9;
        case 'a': return 10;
        case 'b': return 11;
        case 'c': return 12;
        case 'd': return 13;
        case 'e': return 14;
        case 'f': return 15;

        default:
        {
            cerr << "Nesprávný vstup." << endl;
            exit(0);
        }

    }
    
    
    
}
